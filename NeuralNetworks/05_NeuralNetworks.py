# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Neural Networks in scikit-learn
# https://scikit-learn.org/stable/modules/neural_networks_supervised.html
#
# Background: reminder
#
# ## Linear models:
# $\hat{y} = w[0] * x[0] + w[1] * x[1] + ... + w[p] * x[p] + b$

# %%
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split

# help(MLPClassifier)

# %%
cancer = load_breast_cancer()
X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, random_state=0
)

# %%
mlp = MLPClassifier(random_state=42)
mlp.fit(X_train, y_train)
print(f"Accuracy on the training subset: {mlp.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {mlp.score(X_test, y_test):.3f}")
print(f"The maximum per each feature:\n{cancer.data.max(axis=0)}")

# %% [markdown]
# Multi-layer Perceptron classifier
# =================================
#
# -   https://en.wikipedia.org/wiki/Perceptron
# -   https://en.wikipedia.org/wiki/Multilayer_perceptron
#
# ![image0.png](./images/image0.png)
#
# http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
#
# This model optimizes the log-loss function using LBFGS or stochastic gradient descent.

# %%
from sklearn.preprocessing import StandardScaler

# help(StandardScaler)

# %%
scaler = StandardScaler()
X_train_scaled = scaler.fit(X_train).transform(X_train)
X_test_scaled = scaler.fit(X_test).transform(X_test)
print(X_test_scaled)

# %%
mlp = MLPClassifier(max_iter=1000, random_state=42)
mlp.fit(X_train_scaled, y_train)
print(f"Accuracy on the training subset: {mlp.score(X_train_scaled, y_train):.3f}")
print(f"Accuracy on the test subset: {mlp.score(X_test_scaled, y_test):.3f}")

# %%
mlp = MLPClassifier(max_iter=1000, alpha=1, random_state=42)
mlp.fit(X_train_scaled, y_train)
print(f"Accuracy on the training subset: {mlp.score(X_train_scaled, y_train):.3f}")
print(f"Accuracy on the test subset: {mlp.score(X_test_scaled, y_test):.3f}")

# %%
plt.figure(figsize=(20, 5))
plt.imshow(mlp.coefs_[0], interpolation="None", cmap="GnBu")
plt.yticks(range(30), cancer.feature_names)
plt.xlabel("Columns in weight matrix")
plt.ylabel("Input feature")
plt.colorbar()

# %% [markdown]
# Ask: What do the above weights mean?
#
# ## Advantages and Disadvantages of Neural Nets (scikit-learn)

# %% [markdown]
# ### Stronger points:
#
# -   can be used efficiently on large datasets
# -   can build very complex models
# -   many parameters for tuning
# -   flexibility and rapid prototyping
# -   etc.
#
# ### Weaker points:
#
# -   many parameters for tuning
# -   some solvers are scale sensitive
# -   data may need to be pre-processed
# -   etc.
#
# ### Alternatives:
#
# -   theano
# -   tensorflow
# -   keras
# -   lasagna
# -   **from scratch**!

# %% [markdown]
# Here's one from scratch:
# [./nn_simple.py](./nn_simple.py)
