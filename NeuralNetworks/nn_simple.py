#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
PROGRAM INFO:
Note that np.dot() is the dot product, which is a cute and efficient way to represent the summation of the weight-activation products (e.g., neuron summation); for more information see:
https://en.wikipedia.org/wiki/Matrix_multiplication
https://en.wikipedia.org/wiki/Dot_product
hint: think about the basic formula for a neuron!

Variable Definitions
X: Input dataset matrix where each row is a training example

y: Output dataset matrix where each row is a training example

l0: First Layer of the Network, specified by the input data

l1: Second Layer of the Network, otherwise known as the hidden layer

l2: Final Layer of the Network, which is our hypothesis, and should approximate the correct answer as we train.

syn0: First layer of weights, Synapse 0, connecting l0 to l1.

syn1: Second layer of weights, Synapse 1 connecting l1 to l2.

l2_error: This is the amount that the neural network "missed".

l2_delta: This is the error of the network scaled by the confidence. It's almost identical to the error except that very confident errors are muted.

l1_error: Weighting l2_delta by the weights in syn1, we can calculate the error in the middle/hidden layer.

l1_delta: This is the l1 error of the network scaled by the confidence. Again, it's almost identical to the l1_error except that confident errors are muted.
"""

import numpy as np

np.random.seed(1)


def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))


def sigmoid_prime(x):
    return sigmoid(x) * (1.0 - sigmoid(x))


# hyperbolic tangent is often better than sigmoid, and you're welcome to try it, or others
def tanh(x):
    return np.tanh(x)


def tanh_prime(x):
    return 1.0 - np.tanh(x) ** 2


class NN:
    def __init__(self, nI=3, nH=4, nO=1):
        # Randomly initialize our network weights with mean 0
        # This implicitly initializes nodes, which become explicit in runNN
        # first Input layer (nI) has 3 inputs, second Hidden (nH) has 4 neurons
        self.syn0 = 2 * np.random.random((nI, nH)) - 1
        # from the 4 Hidden to the 1 Output (nO)
        self.syn1 = 2 * np.random.random((nH, nO)) - 1

    def runNN(self, X):
        # Feed forward activity through layers 0, 1, and 2
        self.l0 = X
        self.l1 = sigmoid(np.dot(self.l0, self.syn0))
        self.l2 = sigmoid(np.dot(self.l1, self.syn1))
        return self.l2

    def backPropagate(self, y, N):
        # by how much did we miss the target value?
        # could use a variety of errors here, e.g., MSE, etc
        l2_error = y - self.l2
        # in what direction is the target value?
        # were we really sure? if so, don't change too much.
        l2_delta = l2_error * sigmoid_prime(self.l2)
        # how much did each l1 value contribute to the l2 error,
        # according to the weights?
        l1_error = l2_delta.dot(self.syn1.T)
        # in what direction is the target l1?
        # were we really sure? if so, don't change too much.
        l1_delta = l1_error * sigmoid_prime(self.l1)
        # update weights with learning rate = N
        self.syn1 += (self.l1.T.dot(l2_delta)) * N
        self.syn0 += (self.l0.T.dot(l1_delta)) * N

    def train(self, X, y, max_iterations=10000, N=0.8):
        # this is batch now, but could loop each example singly
        for roundNum in range(max_iterations):
            self.runNN(X)
            self.backPropagate(y, N)

    def test(self, X, y):
        final_prediction = self.runNN(X)
        print("Final classification outputs were: ")
        print(final_prediction)
        print("Final classification outputs were supposed to be: ")
        print(y)
        print("Final mean error is: ", np.mean((final_prediction - y) ** 2))
        return np.mean((final_prediction - y) ** 2)


def nonLinExample():
    # A non-linear problem kind of like XOR; this networks could easily solve XOR
    # 4 X arrays, with 3 inputs per X. With each sub-array, xor each value.
    # Each element of the y array is the answ
    X = np.array([[0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 1]])
    # For example, 0xor0xor1 = 1, 0xor1xor1 = 0, etc.
    y = np.array([[1], [0], [0], [1]])
    myNN = NN(3, 4, 1)
    myNN.train(X, y)
    grand_mean_error = myNN.test(X, y)
    print(grand_mean_error)


if __name__ == "__main__":
    nonLinExample()
