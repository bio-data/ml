# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# Machine learning!
# =================
#
# ![image0.png](./images/image0.png)
#
# This is both good (how the models work) and also bad (how one massages scientific results from null to significant for a career)...

# %% [markdown]
# Linear regression
# =================
#
# https://en.wikipedia.org/wiki/Linear_regression
#
# With 1 X (feature)
#
# ![image1.png](./images/image1.png)
#
# Minimize the green lines
#
# A linear function of features $X_1,\dots,X_n$ is a function of the form:
#
# $f^{\overline{w}}(X_1,\dots,X_n) = w_0+w_1 X_1 + \dots + w_n X_n$
#
# We invent a new feature $X_0$ which has value 1, to make it not a
# special case.
#
# $f^{\overline{w}}(X_1,\dots,X_n) = \sum_{i=0}^n w_i X_i$
#
# Aim: predict feature $Y$ from features $X_1, \dots, X_n$.
#
# A feature is a function of an example.
#
# ${X_i}(e)$ is the value of feature $X_i$ on example $e$.
#
# predict a linear function of the input features.
#
# $Y = w_{0} + w_{1} X_{1}(e) + \dots + w_{n} X_{n}(e)$
#
# $Y = \sum_{i=0}^{n} w_{i} X_{i}(e),$
#
# All that can just be performed with the dot product, using the vector of weights and the matrix of samples (or a vector sample).

# %% [markdown]
# ## How the line is "learned" or shuffled into place, is interesting:
# it can be done
# 1. analytically, or
# 2. iteratively/stochastically (my preference).
#
# If anyone is interested in these methods, I can send you slides from more ML-focused classes I have taught, and/or you can read up:
# https://duckduckgo.com/?q=stochastic+gradient+descent+linear+regression

# %% [markdown]
# ## Example linear regression on diabetes dataset
#
# https://scikit-learn.org/stable/datasets/toy_dataset.html#diabetes-dataset
#
# First 10 columns are numeric predictive values
#
# Target column 11 is a quantitative measure of disease progression one year after baseline

# %%
# Example of linear regression
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

help(datasets.load_diabetes)

# %%
diabetes = datasets.load_diabetes(as_frame=True)
print(diabetes.DESCR)

# %%
diabetes

# %%
print(diabetes.feature_names)

# %%
print(diabetes.target)

# %%
# To show a basic line-plot with one feature:

# Load the diabetes dataset
diabetes_X, diabetes_y = datasets.load_diabetes(return_X_y=True)

# Use only one feature
diabetes_X = diabetes_X[:, np.newaxis, 2]

# Split the data into training/testing sets
diabetes_X_train = diabetes_X[:-20]
diabetes_X_test = diabetes_X[-20:]

# Split the targets into training/testing sets
diabetes_y_train = diabetes_y[:-20]
diabetes_y_test = diabetes_y[-20:]

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(diabetes_X_train, diabetes_y_train)

# Make predictions using the testing set
diabetes_y_pred = regr.predict(diabetes_X_test)

# The coefficients
print("Coefficients: \n", regr.coef_)
# The mean squared error
print(f"Mean squared error: {mean_squared_error(diabetes_y_test, diabetes_y_pred):.2f}")
# The coefficient of determination: 1 is perfect prediction
print(f"Coefficient of determination: {r2_score(diabetes_y_test, diabetes_y_pred):.2f}")

# Plot outputs
plt.scatter(diabetes_X_test, diabetes_y_test, color="black")
plt.plot(diabetes_X_test, diabetes_y_pred, color="blue", linewidth=3)

plt.xticks(())
plt.yticks(())

plt.show()

# %%
# Load the diabetes dataset
diabetes_X, diabetes_y = datasets.load_diabetes(return_X_y=True)

# Split the data into training/testing sets
diabetes_X_train = diabetes_X[:-20]
diabetes_X_test = diabetes_X[-20:]

# Split the targets into training/testing sets
diabetes_y_train = diabetes_y[:-20]
diabetes_y_test = diabetes_y[-20:]

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(diabetes_X_train, diabetes_y_train)

# Make predictions using the testing set
diabetes_y_pred = regr.predict(diabetes_X_test)

# The coefficients
print("Coefficients: \n", regr.coef_)
print("Coefficient names: \n", diabetes.feature_names)
# The mean squared error
print(f"Mean squared error: {mean_squared_error(diabetes_y_test, diabetes_y_pred):.2f}")
# The coefficient of determination: 1 is perfect prediction
print(f"Coefficient of determination: {r2_score(diabetes_y_test, diabetes_y_pred):.2f}")
print(diabetes.DESCR)

# %% [markdown]
# # Linear classifier:
# https://en.wikipedia.org/wiki/Linear_classifier
#
# * A generative model is a statistical model of the joint probability distribution $P(X,Y)$ on given observable variable X and target variable Y; Examples of such algorithms include:
#     * Linear Discriminant Analysis (LDA)—assumes Gaussian conditional density models
#     * Naive Bayes classifier with multinomial or multivariate Bernoulli event models.
# * A discriminative model is a model of the conditional probability $P(Y\mid X=x)$ of the target Y, given an observation x. The second set of methods includes discriminative models, which attempt to maximize the quality of the output on a training set. Additional terms in the training cost function can easily perform regularization of the final model. Examples of discriminative training of linear classifiers include:
#     * Logistic regression: maximum likelihood estimation of w → {\displaystyle {\vec {w}}} {\vec {w}} assuming that the observed training set was generated by a binomial model that depends on the output of the classifier.
#     * Perceptron: an algorithm that attempts to fix all errors encountered in the training set
#     * Fisher's Linear Discriminant Analysis—an algorithm (different than "LDA") that maximizes the ratio of between-class scatter to within-class scatter, without any other assumptions. It is in essence a method of dimensionality reduction for binary classification.
#     * Support vector machine (SVM) an algorithm that maximizes the margin between the decision hyperplane and the examples in the training set.

# %% [markdown]
# ## For example
# Instead of minimizing blue lines (like above), minimize error in classificiation with a squashed linear function like a step function, logistic).
#
# ![image2.png](./images/image2.png)
#
# Solid and empty dots can be correctly classified by any number of linear classifiers.
#
# H1 (blue) classifies them correctly, as does H2 (red).
#
# H2 could be considered better since it is furthest from both groups.
#
# H3 (green) fails to correctly classify the dots.
#
# If the input feature vector to the classifier is a real vector ${\vec {x}}$, then the output score is
#
# $y=f({\vec {w}}\cdot {\vec {x}})=f\left(\sum _{j}w_{j}x_{j}\right),$
#
# where $\vec {w}$ is a real vector of weights and f is a function that converts the dot product of the two vectors into the desired output (e.g., step function below)
#
# (In other words, $\vec {w}$ is a one-form or linear functional mapping $\vec {x}$ onto R.)
# The weight vector $\vec {w}$ is learned from a set of labeled training samples.
# Often f is a threshold function, which maps all values of ${\vec {w}}\cdot {\vec {x}}$ above a certain threshold to the first class and all other values to the second class; e.g.,
#
# $f(\mathbf {x} )={\begin{cases}1&{\text{if }}\ \mathbf {w} ^{T}\cdot \mathbf {x} >\theta ,\\0&{\text{otherwise}}\end{cases}}$
#
# The superscript $T$ indicates the transpose and $\theta$ is a scalar threshold.
# A more complex f might give the probability that an item belongs to a certain class.
#
# For a probability of being in a class, a logistic function can be used):
#
# Question: Why is that helpful?

# %% [markdown]
# ![image3.png](./images/image3.png)

# %% [markdown]
# ## Logistic regression
#
# https://en.wikipedia.org/wiki/Logistic_regression
#
# http://scikit-learn.org/stable/modules/linear_model.html#logistic-regression
#
# http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html#sklearn.linear_model.LogisticRegression
#
# ### Why not linear regression?
#
# * When the response variable has only 2 possible values, it is desirable to have a model that predicts the value either as 0 or 1 or as a probability score that ranges between 0 and 1.
# * Linear regression does not have this capability.
# * Because, If you use linear regression to model a binary response variable, the resulting model may not restrict the predicted Y values within 0 and 1.
# * Logistic regression, despite its name, is a linear model for classification rather than regression.
# * Logistic regression is also known in the literature as logit regression

# %% [markdown]
# ![logistic.png](./images/logistic.png)

# %%
from sklearn.datasets import load_breast_cancer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np

# help(LogisticRegression)

# %%
cancer = load_breast_cancer()

X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, stratify=cancer.target, random_state=42
)

log_reg = LogisticRegression()

log_reg.fit(X_train, y_train)

# %%
print(f"Accuracy on the training subset: {log_reg.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {log_reg.score(X_test, y_test):.3f}")

# %% [markdown]
# **Regularization** (punish complex models):
# https://en.wikipedia.org/wiki/Regularization_(mathematics)
#
# -   prevention of overfitting
# -   L1 - assumes only a few features are important
# -   L2 - does not assume only a few features are important - used by default in scikit-learn LogisticRegression
#
# **'C'**:
#
# -   parameter to control the strength of regularization
# -   lower C =\> log\_reg adjusts to the majority of data points.
# -   higher C =\> correct classification of each data point.
#
# ![image5.png](./images/image5.png)
#
# The green and blue functions both incur zero loss on the given data points.
# A learned model can be induced to prefer the green function, which may generalize better to more points drawn from the underlying unknown distribution, by adjusting $\lambda$, the weight of the regularization term.

# %%
log_reg100 = LogisticRegression(C=100)

log_reg100.fit(X_train, y_train)

print(f"Accuracy on the training subset: {log_reg100.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {log_reg100.score(X_test, y_test):.3f}")

# %%
log_reg001 = LogisticRegression(C=0.01)
log_reg001.fit(X_train, y_train)
print(f"Accuracy on the training subset: {log_reg001.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {log_reg001.score(X_test, y_test):.3f}")

# %% [markdown]
# ### Linear Models (in general):
#
# #### <center> y = w \* x + b </center>
#
# -   w - slope (or coefficient) - accessed via <raw>.coef\_</raw>
# -   b - offset (or intercept) - access via <raw>.intercept\_</raw>
# -   w and b are learned parameters
# -   y - prediction (decision)
#
# Example (for a dataset with only 1 input features): ŷ = w\[0\] \* x\[0\] + b
#
# ### For Logistic Regression (specifically):
#
# #### <center> ŷ = w\[0\] \* x\[0\] + w\[1\] \* x\[1\] + ... + w\[p\] \* x\[p\] + b \> 0</center>

# %% [markdown]
# Questions
# * What weights are learned?
# * Which features show up as important?

# %%
log_reg.coef_

# %%
plt.figure(figsize=(10, 10))

plt.plot(log_reg.coef_.T, "o", label="C=1")

plt.plot(log_reg100.coef_.T, "^", label="C=100")

plt.plot(log_reg001.coef_.T, "v", label="C=0.01")

plt.xticks(range(cancer.data.shape[1]), cancer.feature_names, rotation=90)
plt.hlines(0, 0, cancer.data.shape[1])
plt.ylim(-5, 5)
plt.xlabel("Coefficient Index")
plt.ylabel("Coefficient Magnitude")
plt.legend()

# %% [markdown]
# ## Cahoot-14-4

# %% [markdown]
# Ask:
# -   is this really a measure of importance of each feature?
# -   what should we do about such information?
# -   Are values with bigger weights actually more relevant?

# %% [markdown]
# Ask: What if the data are not linearly distributed or seprable?
# ![image6.png](./images/image6.png)

# %% [markdown]
# ![image7.png](./images/image7.png)
#
# Ask: how could we try to capture this categorization?
#
# More to come with SVMs and Perceptron neural networks (much like linear regressions actually!)

# %% [markdown]
# ## Naive Bayes
# Another type of linear classifier.
#
# Build a probabality table based on the observed data, use bayes rule to make an inference on a new data vector to predict from.
#
# http://scikit-learn.org/stable/modules/naive_bayes.html
#
# https://en.wikipedia.org/wiki/Naive_Bayes_classifier

# %%
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score

# help(GaussianNB)

# %%
# Load dataset
data = load_breast_cancer()

# Organize our data
label_names = data["target_names"]
labels = data["target"]

feature_names = data["feature_names"]
features = data["data"]

# Split our data
train, test, train_labels, test_labels = train_test_split(
    features, labels, test_size=0.33, random_state=42
)

# Initialize our classifier
gnb = GaussianNB()

# %%
# Train our classifier
model = gnb.fit(train, train_labels)

# Make predictions
preds = gnb.predict(test)
print(preds)
print(test_labels)
# Evaluate accuracy
print(accuracy_score(test_labels, preds))
print(gnb.score(train, train_labels))
print(gnb.score(test, test_labels))

# %%
# It is possible to save a model in the scikit by using Python’s built-in persistence model, namely pickle:

import pickle

s = pickle.dumps(model)
model2 = pickle.loads(s)
model2.predict(test)
