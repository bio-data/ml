# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown] _cell_guid="43b9747c-d81e-4a9a-bfd0-80b8eb89aa54" _uuid="87acfbd88239de13e70d249387df1bdfe40a7f10"
# # Classifying different types of Leukemia:
# The dataset:
# https://www.kaggle.com/datasets/crawford/gene-expression
#
# http://www.marcottelab.org/users/BCH339N_2016/AMLALLclassification.pdf
#
# The first major demonstration of doing this in the scientific literature (paper above, data below)

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
labels = pd.read_csv("actual.csv", index_col="patient")
test = pd.read_csv("data_set_ALL_AML_independent.csv")
train = pd.read_csv("data_set_ALL_AML_train.csv")
train.head()

# %%
labels

# %% [markdown]
# <h2>Data Cleaning and Preparation</h2>

# %%
cols = [col for col in test.columns if "call" in col]
test = test.drop(cols, 1)

cols = [col for col in train.columns if "call" in col]
train = train.drop(cols, 1)

train.head()

# %%
train = train.T
test = test.T
train.head()

# %%
train = train.drop(["Gene Description", "Gene Accession Number"])
test = test.drop(["Gene Description", "Gene Accession Number"])

train.head()

# %%
labels = labels.replace({"ALL": 0, "AML": 1})
labels_train = labels[labels.index <= 38]
labels_test = labels[labels.index > 38]

train = train.replace(np.inf, np.nan)
train = train.fillna(value=train.values.mean())

test = test.replace(np.inf, np.nan)
test = test.fillna(value=test.values.mean())

# %% [markdown]
# We join the training and test sets in order to get the same columns after PCA.

# %%
df_all = train.append(test, ignore_index=True)
df_all

# %%
labels

# %%
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

# %%
X_train, X_test, y_train, y_test = train_test_split(
    df_all, labels, stratify=labels, random_state=50
)
print("X_train", X_test)
print("X_test", X_test)
print("y_train", y_train)
print("y_test", y_test)

# %%
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)
print(f"Accuracy of KNN n-5, on the training set: {knn.score(X_train, y_train):.3f}")
print(f"Accuracy of KNN n-5, on the test set: {knn.score(X_test, y_test):.3f}")

# %%
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

predictions = knn.predict(X_test)

print("Test Accuracy  :: ", accuracy_score(y_test, predictions))
print("\n\nConfusion matrix:")
confusion_matrix(y_test, predictions)
