# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# Support Vector Machines (SVMs) in scikit-learn
# ==============================================
#
# https://en.wikipedia.org/wiki/Support_vector_machine
#
# Supervised:
# -----------
#
# - Maximum-margin hyperplane and margins for an SVM trained with samples from two classes.
# - Samples on the margin are called the support vectors
#
# Unsupervised
# ------------
#
# - When data is unlabelled, supervised learning is not possible, and an unsupervised learning approach is required, which attempts to find natural clustering of the data to groups, and then map new data to these formed groups.
# - The support-vector clustering algorithm, applies the statistics of support vectors, developed in the support vector machines algorithm, to categorize unlabeled data, and is one of the most widely used clustering algorithms in industrial applications.
#
# ![image0.png](./images/image0.png)
#
# - for classification and regression (SVCs, SVRs)
# - can be applied on linear and non-linear data
# - look for the best separating line or decision boundary
# - look for the largest margin
#
# In addition to performing linear classification, SVMs can efficiently perform a non-linear classification using what is called the kernel trick, implicitly mapping their inputs into high-dimensional feature spaces.
#
# https://en.wikipedia.org/wiki/Kernel_trick
#
# In its simplest form, the kernel trick means transforming data into another dimension that has a clear dividing margin between classes of data.
#
# - In machine learning, kernel methods are a class of algorithms for pattern analysis, whose best known member is the support vector machine (SVM).
# - The general task of pattern analysis is to find and study general types of relations (for example clusters, rankings, principal components, correlations, classifications) in datasets.
# - In its simplest form, the kernel trick means transforming data into another dimension that has a clear dividing margin between classes of data.
# - For many algorithms that solve these tasks, the data in raw representation have to be explicitly transformed into feature vector representations via a user-specified feature map:
#     - in contrast, kernel methods require only a user-specified kernel,
#     - i.e., a similarity function over pairs of data points in raw representation.

# %% [markdown]
# - Still just create linear hyperplane for separation
# - Map inputs into higher dimensional space via kernel trick
# - Often data not linearly separable will be in higher dimensional space
# - The higher dimensional linear hyperplane is actually non-linear in the original space
# - It uses functions of the original inputs as the inputs of the linear function.
# - These functions are called kernel functions. Many possible kernel functions exist.
# - An example kernel function from original features $x_1 , x_2$ into a higher dimensional 3D space is:
#    - $x_2^2 , x_2^2 , \sqrt{2 x_1 x_2}$
#
# ![](./images/086f1eb7b67373e6c61f9a57a16ec350904cc82c.png)

# %% [markdown]
# ![](./images/903d098faf72d289d9e45a8852516d8b1c83106f.png)

# %% [markdown]
# ![](./images/c214e38fc6cb50d473689cf76b989b0a0fa38439.png)

# %% [markdown]
# ### Commonly used kernels:
#
# - linear
# - polynomial
# - radial basis function (RBF) - Gaussian RBF
# - sigmoid
# - etc.
#
# ![image1.png](./images/image1.png)

# %% [markdown]
# ![image2.png](./images/image2.png)
#
# - SVM with kernel given by $\varphi ((a, b)) = (a, b, a^2 + b^2)$ and thus $K(x, y) = x \cdot y + x^2 y^2$.
# - The training points are mapped to a 3-dimensional space where a separating hyperplane can be easily found.

# %%
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_breast_cancer

help(SVC)

# %%
cancer = load_breast_cancer()
X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, random_state=0
)

svm = SVC()
svm.fit(X_train, y_train)

print(f"The accuracy on the training subset: {svm.score(X_train, y_train):.3f}")
print(f"The accuracy on the test subset: {svm.score(X_test, y_test):.3f}")

# %%
import matplotlib.pyplot as plt

plt.plot(X_train.min(axis=0), "o", label="Min")
plt.plot(X_train.max(axis=0), "v", label="Max")
plt.xlabel("Feature Index")
plt.ylabel("Feature Magnitude in Log Scale")
plt.yscale("log")
plt.legend(loc="upper right")

# %%
min_train = X_train.min(axis=0)
range_train = (X_train - min_train).max(axis=0)

X_train_scaled = (X_train - min_train) / range_train

print(f"Minimum per feature\n{X_train_scaled.min(axis=0)}")
print(f"Maximum per feature\n{X_train_scaled.max(axis=0)}")

# %%
X_test_scaled = (X_test - min_train) / range_train

svm = SVC()
svm.fit(X_train_scaled, y_train)

# %%
print(f"The accuracy on the training subset: {svm.score(X_train_scaled, y_train):.3f}")
print(f"The accuracy on the test subset: {svm.score(X_test_scaled, y_test):.3f}")

# %%
svm = SVC(C=1000)
svm.fit(X_train_scaled, y_train)

print(f"The accuracy on the training subset: {svm.score(X_train_scaled, y_train):.3f}")
print(f"The accuracy on the test subset: {svm.score(X_test_scaled, y_test):.3f}")

# %% [markdown]
# # Advantages and Disadvantages of SVMs (scikit-learn)

# %% [markdown]
# ### Stronger points:
# - they are versatile
# - can build complex decision boundaries on low-dimensional data
# - can work well on high-dimensional data with relatively small sample size
# - etc.
#
# ### Weaker points:
# - don't perform well on high-dimensional data with many samples (i.e. \> 100k)
# - preprocessing may be required =\> implies knowledge and understanding of hyper-parameters
# - harder to inspect and visualize
# - etc.
#
# ### Alternatives:
# - DT and Random Forests (require less/no preprocessing of data, easier to understand, inspect, and visualize)
#
# ### Good practices:
# - data scaling
# - other pre-processing
# - choosing an appropriate kernel
# - tuning hyper-parameters: C, gamma, etc.

# %% [markdown]
# Uncertainty Estimation
# -----------------------------------------

# %% [markdown]
# ### The Decision Function

# %%
print(f"The decision function is:\n\n{svm.decision_function(X_test_scaled[:20])}")

# %%
print(
    f"Thresholded decision function:\n\n{svm.decision_function(X_test_scaled[:20]) > 0}"
)

# %%
print(svm.classes_)

# %% [markdown]
# ### Predicting Probabilities

# %%
print(svm)

# %%
svm = SVC(C=1000, probability=True)
svm.fit(X_train_scaled, y_train)

print(
    f"Predicted probabilities for the samples (malignant and benign):\n\n{svm.predict_proba(X_test_scaled[:20])}"
)

# %%
svm.predict(X_test_scaled)
