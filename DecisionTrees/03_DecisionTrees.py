# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Decisions tree
# - http://scikit-learn.org/stable/modules/tree.html
# - https://en.wikipedia.org/wiki/Decision_tree_learning
# - https://en.wikipedia.org/wiki/Decision_tree_learning#Gini_impurity
#
# - Decision Trees (DTs) are a non-parametric supervised learning method used for classification and regression.
# - The goal is to create a model that predicts the value of a target variable by learning simple decision rules inferred from the data features.
# - Tree models where the target variable can take a discrete set of values are called classification trees
# - In these tree structures, leaves represent class labels and branches represent conjunctions of features that lead to those class labels.
#
# ![image0.png](./images/image0.png)

# %% [markdown]
# - Choose a feature to "split" on, perhaps the one that gives the least error (a myopic split).
# - Keep trying more features, stop at some depth.
# - Which test to split on isn’t defined.
# - Often we use myopic split: which single split gives smallest error;
# - if the learner were only allowed one split, which single split would result in the best classification?
# - values or split values into half.
# - More complex tests are possible.
#
# ![image1.png](./images/image1.png)

# %%
import matplotlib.pyplot as plt
import numpy as np

# %%
from sklearn.datasets import load_breast_cancer
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split

# help(DecisionTreeClassifier)

# %%
cancer = load_breast_cancer()
X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, stratify=cancer.target, random_state=42
)

# %%
tree = DecisionTreeClassifier(random_state=0)
tree.fit(X_train, y_train)
print(f"Accuracy on the training subset: {tree.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {tree.score(X_test, y_test):.3f}")

# %%
tree = DecisionTreeClassifier(max_depth=4, random_state=0)
tree.fit(X_train, y_train)
print(f"Accuracy on the training subset: {tree.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {tree.score(X_test, y_test):.3f}")

# %%
# import graphviz
from sklearn.tree import export_graphviz

export_graphviz(
    tree,
    out_file="cancertree.dot",
    class_names=["malignant", "benign"],
    feature_names=cancer.feature_names,
    impurity=False,
    filled=True,
)

# %% [markdown]
# ![](./images/451c82986ac0ba689d763c9f2fcfcf1656f36372.png)
#
# Ask and look: how does this compare to the regression weights?

# %%
print(f"Feature importances: {tree.feature_importances_}")
print(type(tree.feature_importances_))

# %%
print(cancer.feature_names)

# %%
print(sorted(list(zip(tree.feature_importances_, cancer.feature_names)), reverse=True))

# %%
n_features = cancer.data.shape[1]
plt.figure(figsize=(10, 10))
plt.barh(range(n_features), tree.feature_importances_, align="center")
plt.yticks(np.arange(n_features), cancer.feature_names)
plt.xlabel("Feature Importance")
plt.ylabel("Feature")
plt.show()

# %% [markdown]
# ### Decision Trees
# are nice because:
# - easy to view and understand
# - no need to pre-process, normalize, scale, and/or standardize features
#
# ### Paramaters to work with
#
# - max\_depth
# - min\_samples\_leaf, max\_samples\_leaf
# - max\_leaf\_nodes
# - etc.
#
# The deeper the tree, the more complex the decision rules and the fitter the model.

# %% [markdown]
# ### This algorithm can overfit the data.
#
# - tendency to overfit
# - poor generalization
#
# This occurs when noise and correlations in the training set that are not reflected in the data as a whole. To handle over-fitting:
#
# - restrict the splitting, and split only when the split is useful.
# - allow unrestricted splitting and prune the resulting tree where it makes unwarranted distinctions.
# - learn multiple trees and average them.
#
# Possible work-around: ensembles of decision trees

# %% [markdown]
# Some advantages of decision trees are:
# --------------------------------------
#
# - Simple to understand and to interpret. Trees can be visualised.
# - Requires little data preparation.
#   Other techniques often require data normalisation, dummy variables need to be created and blank values to be removed.
#     - Note however that this module does not support missing values.
# - The cost of using the tree (i.e., predicting data) is logarithmic in the number of data points used to train the tree.
# - Able to handle both numerical and categorical data. Other techniques are usually specialised in analysing datasets that have only one type of variable.
# - Able to handle multi-output problems.
# - Uses a white box model. If a given situation is observable in a model, the explanation for the condition is easily explained by boolean logic.
#     - By contrast, in a black box model (e.g., in an artificial neural network), results may be more difficult to interpret.
# - Possible to validate a model using statistical tests.
#     - That makes it possible to account for the reliability of the model.
# - Performs well even if its assumptions are somewhat violated by the true model from which the data were generated.
#
# The disadvantages of decision trees include:
# --------------------------------------------
#
# - Decision-tree learners can create over-complex trees that do not generalise the data well.
#     - This is called overfitting.
#     - Mechanisms such as pruning (not currently supported), setting the minimum number of samples required at a leaf node or setting the maximum depth of the tree are necessary to avoid this problem.
# - Decision trees can be unstable because small variations in the data might result in a completely different tree being generated.
#     - This problem is mitigated by using decision trees within an ensemble.
# - The problem of learning an optimal decision tree is known to be NP-complete under several aspects of optimality and even for simple concepts.
#     - Consequently, practical decision-tree learning algorithms are based on heuristic algorithms such as the greedy algorithm where locally optimal decisions are made at each node.
#     - Such algorithms cannot guarantee to return the globally optimal decision tree.
#     - This can be mitigated by training multiple trees in an ensemble learner, where the features and samples are randomly sampled with replacement.
# - There are concepts that are hard to learn because decision trees do not express them easily, such as XOR, parity or multiplexer problems.
# - Decision tree learners create biased trees if some classes dominate.
#     - It is therefore recommended to balance the dataset prior to fitting with the decision tree.

# %% [markdown]
# ## Tips on practical use
# - Decision trees tend to overfit on data with a large number of features. Getting the right ratio of samples to number of features is important,
# - Consider performing dimensionality reduction (PCA, ICA, or Feature selection) beforehand to give your tree a better chance of finding features that are discriminative.
# - Visualise your tree as you are training by using the export function.
#   Use max\_depth=3 as an initial tree depth to get a feel for how the tree is fitting to your data, and then increase the depth.
# - Remember that the number of samples required to populate the tree doubles for each additional level the tree grows to.
#   Use max\_depth to control the size of the tree to prevent overfitting.
# - Use min\_samples\_split or min\_samples\_leaf to control the number of samples at a leaf node.
#   A very small number will usually mean the tree will overfit, whereas a large number will prevent the tree from learning the data.
#   Try min\_samples\_leaf=5 as an initial value.
#   If the sample size varies greatly, a float number can be used as percentage in these two parameters.
#   The main difference between the two is that min\_samples\_leaf guarantees a minimum number of samples in a leaf, while min\_samples\_split can create arbitrary small leaves, though min\_samples\_split is more common in the literature.
# - Balance your dataset before training to prevent the tree from being biased toward the classes that are dominant.
#   Class balancing can be done by sampling an equal number of samples from each class, or preferably by normalizing the sum of the sample weights (sample\_weight) for each class to the same value.
#   Also note that weight-based pre-pruning criteria, such as min\_weight\_fraction\_leaf, will then be less biased toward dominant classes than criteria that are not aware of the sample weights, like min\_samples\_leaf.
# - If the samples are weighted, it will be easier to optimize the tree structure using weight-based pre-pruning criterion such as min\_weight\_fraction\_leaf, which ensure that leaf nodes contain at least a fraction of the overall sum of the sample weights.
# - All decision trees use np.float32 arrays internally.
#   If training data is not in this format, a copy of the dataset will be made.
# - If the input matrix X is very sparse, it is recommended to convert to sparse csc\_matrix before calling fit and sparse csr\_matrix before calling predict.
#   Training time can be orders of magnitude faster for a sparse matrix input compared to a dense matrix when features have zero values in most of the samples.

# %% [markdown]
# ## Decision trees can also do regression
# https://scikit-learn.org/stable/modules/tree.html#regression
