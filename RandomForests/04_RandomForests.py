# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Ensemble learning
#
# https://en.wikipedia.org/wiki/Ensemble_learning
#
# In statistics and machine learning, ensemble methods use multiple learning algorithms to obtain better predictive performance than could be obtained from any of the constituent learning algorithms alone.
#
# ![image0.png](./images/image0.png)

# %% [markdown]
# ![image1.png](./images/image1.png)

# %% [markdown]
# ![image2.png](./images/image2.png)

# %% [markdown]
# Random forests
# ==============
#
# This is a random forest...
#
# ![image3.png](./images/image3.png)
#
# * https://scikit-learn.org/stable/modules/ensemble.html#forests-of-randomized-trees
# * https://en.wikipedia.org/wiki/Random_forest
#
# Random forests are also a type of ensemble learning (there are others as well).
# * Random decision forests correct for decision trees' habit of overfitting to their training set.
# * Construct a multitude of decision trees at training time, and output the class that is the **mode** of the classes (classification) or mean prediction (regression) of the individual trees.

# %%
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_breast_cancer
import matplotlib.pyplot as plt
import numpy as np

# help(RandomForestClassifier)

# %%
cancer = load_breast_cancer()
X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, random_state=0
)

# %%
forest = RandomForestClassifier(n_estimators=100, random_state=0)
forest.fit(X_train, y_train)
print(f"Accuracy on the training subset: {forest.score(X_train, y_train):.3f}")
print(f"Accuracy on the test subset: {forest.score(X_test, y_test):.3f}")

# %%
n_features = cancer.data.shape[1]
plt.barh(range(n_features), forest.feature_importances_, align="center")
plt.yticks(np.arange(n_features), cancer.feature_names)
plt.xlabel("Feature Importance")
plt.ylabel("Feature")
plt.show()

# %% [markdown]
# ### Potential Advantages of Random Forests
#
# -   powerful and widely implemented
# -   perform well with default settings
# -   dont require scaling of the data
# -   randomization makes them better than single DT
#
# ### Parameters to Tune
#
# -   n\_jobs - number of cores to use for training (n\_jobs=-1, for all
#     cores)
# -   n\_estimators - how many trees to use (more is always better)
# -   max\_depth, for pre-pruning
# -   max\_features, for randomization
#     -   max\_features = sqrt(n\_features), for classification
#     -   max\_features = log2(n\_features), for regression
# -   etc.
#
# ### Potential Disadvantages of Random Forests
#
# -   not so good performance on very high dimensional and sparse data
#     (text data)
# -   large datasets require more resources for training (time, CPUs,
#     etc).
# -   cannot be visualized as well as single DT
