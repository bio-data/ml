#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple KNN from scratch that mimicks sklearn
"""

import math
import collections
import numpy as np
from sklearn.datasets import load_breast_cancer
from typing import Callable, Any
from numbers import Number

# from typing import Union
# numeric = Union[int, float]


def mean(labels: list[Number]) -> float:
    return sum(labels) / len(labels)


def mode(labels: list[Any]) -> Any:
    return collections.Counter(labels).most_common(1)[0][0]


def euclidean_distance(vector1: np.ndarray, vector2: np.ndarray) -> float:
    sum_squared_distance = 0.0
    for i in range(len(vector1)):
        sum_squared_distance += math.pow(vector1[i] - vector2[i], 2)
    return math.sqrt(sum_squared_distance)


class KNN:
    def __init__(
        self,
        n_neighbors: int,
        distance_fn: Callable[[list[Number]], Number],
        choice_fn: Callable[[list[Any]], Any],
    ) -> None:
        """
        Initialize KNN object with basic options:
            n_neighbors - the number of neigbors to match/search
            distance_fn: euclidean, or other
            choice_fn: mean, mode, median, etc.
        """
        self.k = n_neighbors
        self.distance_fn = distance_fn
        self.choice_fn = choice_fn

    def fit(self, X: np.ndarray, y: np.ndarray) -> None:
        """
        For KNN, this is a really minimal "fitting" process.
        Just copy the data vectors (X) and target vector (y)
        into class object storage.
        """
        self.data = X
        self.target = y

    def predict(self, X: np.ndarray) -> tuple[list[tuple[float, int]], list[Any], Any]:
        """
        The actual leg work in this lazy KNN is done at prediction time:
        1. Pairwise comparison of new un-labeled X in all X-y/data-target using distance_fn
        2. to produce k top hits,
        3. of which the winner is produced via choice_fn.
        Returns:
            tuple of tuple of (distances and indices)
            list of candidate targets
            choice of those candidates
        """
        neighbor_distances_and_indices = []

        # 3. For each example in the data
        for index, example in enumerate(self.data):
            # 3.1 Calculate the distance between the existing data and query example X
            distance = self.distance_fn(vector1=example, vector2=X)

            # 3.2 Add the distance and the index of the example to an ordered collection
            neighbor_distances_and_indices.append((distance, index))

        # 4. Sort the ordered collection of distances and indices from
        # smallest to largest (in ascending order) by the distances
        sorted_neighbor_distances_and_indices = sorted(neighbor_distances_and_indices)

        # 5. Pick the first K entries from the sorted collection
        k_nearest_distances_and_indices = sorted_neighbor_distances_and_indices[
            : self.k
        ]

        # 6. Get the labels of the selected K entries
        k_nearest_labels = [
            self.target[i] for distance, i in k_nearest_distances_and_indices
        ]

        # 7. If regression (choice_fn = mean), return the average of the K labels
        # 8. If classification (choice_fn = mode), return the mode of the K labels
        return (
            k_nearest_distances_and_indices,
            k_nearest_labels,
            self.choice_fn(labels=k_nearest_labels),
        )


def main() -> None:
    # a prediction instance
    cancer = load_breast_cancer()

    # Initialize the KNN object with basic paramaters
    knn_object = KNN(n_neighbors=3, distance_fn=euclidean_distance, choice_fn=mode)

    # Acutally copy data into object
    knn_object.fit(X=cancer.data, y=cancer.target)

    # Example prediction on 2nd data element
    clf_k_nearest_neighbors, clf_k_nearest_classes, clf_prediction = knn_object.predict(
        X=cancer.data[1]
    )

    print(f"Top {knn_object.k} neighbors: [((Euclidean dist, index), class)")
    for neighbor in zip(clf_k_nearest_neighbors, clf_k_nearest_classes):
        print(neighbor)
    # Notice how the actual data point is closest to itself...
    # that's peeking/cheating
    print("\nPredicted class: ", clf_prediction)

    # More cheating:
    print("\nAn illustration of percent correct... when we test on the train set")
    correct = 0
    total = len(cancer.data)
    for element in range(total):
        _, _, clf_prediction = knn_object.predict(X=cancer.data[element])
        if cancer.target[element] == clf_prediction:
            correct += 1
    print("Percent correct:", correct / total)


if __name__ == "__main__":
    main()
