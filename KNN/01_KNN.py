# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# Supervised Learning
# ------------------------------------
#
# Machine learning adjusts paramaters to that of a training data set, applies them to new test data.
# * making inferences from labeled data.
#
# #### 1. Classification (categorical data)
# * binary classification (example: tumors are benign or malignant)
# * multiclass classification (Example: books of types like maths, physics, stats, psychology, etc.)
# * example algorithms:
#     * KNN, Linear Models, Decision Trees, SVMs, etc.
#
# #### 2. Regression (continuous data)
# * predicting: income, price of stock, age, and other continous data
# * example algorithms: KNN, Linear Models, Decision Trees, SVMs, etc.
# * Linear models (LinReg, LogReg, Lasso, Ridged, etc) make predictions according to a linear function of the input features.
#
# Many ML algorithms (including those specified above) can be used for both classification and regression.

# %% [markdown]
# # Cahoot 14-1

# %% [markdown]
# # Training and Testing
# ![image0.png](./images/image0.png)
#
# -   Build Phase
#     -   Creating dataset
#     -   Handling missing values
#     -   Splitting data into train and test datasets
#     -   Training classifier
# -   Operational Phase
#     -   Perform predictions
#     -   Accuracy calculations
#         -   Train Accuracy
#         -   Test Accuracy
#
# WhyQ: Why do we use a train and a test set?
# ===========================================

# %% [markdown]
# Make sure to read:
#
# -   <a href="http://www.scipy-lectures.org/packages/scikit-learn/index.html" class="uri">http://www.scipy-lectures.org/packages/scikit-learn/index.html</a>
# -   <a href="https://scikit-learn.org/stable/tutorial/index.html" class="uri">https://scikit-learn.org/stable/tutorial/index.html</a>
#     -   <a href="http://scikit-learn.org/stable/tutorial/basic/tutorial.html" class="uri">http://scikit-learn.org/stable/tutorial/basic/tutorial.html</a>
#     -   <a href="http://scikit-learn.org/stable/tutorial/statistical_inference/index.html" class="uri">http://scikit-learn.org/stable/tutorial/statistical_inference/index.html</a>
#
# ![image1.png](./images/image1.png)

# %% [markdown]
# Cancer classification
# =====================

# %%
from sklearn.datasets import load_breast_cancer
from sklearn.neighbors import KNeighborsClassifier
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# %%
# use sklearn's copy
# data = pd.read_csv("breast-cancer-wisconsin-data.csv")
# data

# %%
cancer = load_breast_cancer()
cancer

# %%
type(cancer.data)

# %%
cancer.keys()

# %%
cancer.items()

# %%
print(cancer.DESCR)

# %%
cancer.data

# %%
cancer.data.shape

# %%
print(cancer.feature_names)

# %%
print(cancer.target_names)

# %%
cancer.target

# %% [markdown]
# Process Outline (for many ML projects)
# ======================================
#
# #### 1. Get the data (pre-process it)
#
# #### 2. Pick an algorithm (classifier)
#
# #### 3. Train the algorithm. Verify accuracy. Optimize.
#
# #### 4. Predict

# %% [markdown]
# <a href="https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm" class="uri">https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm</a>
#
# <a href="http://scikit-learn.org/stable/modules/neighbors.html" class="uri">http://scikit-learn.org/stable/modules/neighbors.html</a>
#
# - The k-nearest neighbors algorithm (k-NN) is used for classification and regression.
# - In both cases, the input consists of the k closest training examples in the feature space.
# - The output depends on whether k-NN is used for classification or regression:
#     - In k-NN **classification**, the output is a class membership.
#         - An object is classified by a majority vote of its neighbors, with the object being assigned to the class most common among its k nearest neighbors.
#         - k is a positive integer, typically small.
#         - If k = 1, then the object is simply assigned to the class of that single nearest neighbor.
#     - In k-NN **regression**, the output is the property value for the object.
#         * This value is the average of the values of k nearest neighbors.
# - k-NN is a type of instance-based learning, or lazy learning, where the function is only approximated locally, and all computation is deferred until classification.
#     * The k-NN algorithm is among the simplest of all machine learning algorithms.
#
# ![image2.png](./images/image2.png)
#
# -   Example of k-NN classification.
# -   The test sample (green circle) should be classified either to the
#     first class of blue squares or to the second class of red triangles.
# -   If k = 3 (solid line circle) it is assigned to the second class
#     because there are 2 triangles and only 1 square inside the inner
#     circle.
# -   If k = 5 (dashed line circle) it is assigned to the first class (3
#     squares vs. 2 triangles inside the outer circle).

# %% [markdown]
# -   Need a distance metric between examples.
# -   Given a new example, find the $k$ nearest neighbors of that example.
# -   Predict the classification by using the mode, median, or interpolating between the neighbors.
# -   Often want $k>1$ because there can be errors in the case base.
#
# **Euclidean Distance**
#
# https://en.wikipedia.org/wiki/Euclidean_distance
#
# -   Define a metric for each dimension (convert the values to a numerical scale).
# -   The Euclidean distance between examples $x$ and $y$ is:
#
# \$ d(x,y) = \\sqrt{\\sum\_{A} w\_{A} (x\_{A} - y\_{A})^2} \$
#
# -   $_{A}$ is the numerical value of attribute $A$ for example $x$
#
# -   $w_{A}$ is a nonnegative real-valued parameter that specifies the relative weight of attribute $A$.
#     * (Only if you want to weight dimensions).
#
# -   Both for classification and regression, a useful technique can be used to assign weight to the contributions of the neighbors, so that the nearer neighbors contribute more to the conclusion than the more distant ones.
#     * For example, a common weighting scheme consists in giving each neighbor a weight of 1/d, where d is the distance to the neighbor.
#
# -   **Discuss**: why would normalizing, or lack theoreof, relate to weighting?

# %% [markdown]
# # Cahoot 14-2

# %%
# KNN Classifier Overview
from sklearn.datasets import load_breast_cancer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt

# %%
cancer

# %%
help(train_test_split)

# %%
X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, stratify=cancer.target, random_state=42
)

print("X_train", X_train)
print("X_test", X_test)
print("y_train", y_train)
print("y_test", y_test)

# %%
# Notice that it defaults to 5 neighbors
help(KNeighborsClassifier)

# %%
knn = KNeighborsClassifier()

knn.fit(X_train, y_train)

# %%
print(f"Accuracy of KNN n-5, on the training set: {knn.score(X_train, y_train):.3f}")
print(f"Accuracy of KNN n-5, on the test set: {knn.score(X_test, y_test):.3f}")

# %%
# Resplit the data, with a different randomization
# (inspired by Muller & Guido ML book - https://www.amazon.com/dp/1449369413/)

X_train, X_test, y_train, y_test = train_test_split(
    cancer.data, cancer.target, stratify=cancer.target, random_state=66
)

# Create two lists for training and test accuracies
training_accuracy = []
test_accuracy = []

# Define a range of 1 to 10 (included) neighbors to be tested
neighbors_settings = range(1, 30)

# Loop with the KNN through the different number of neighbors to determine the most appropriate (best)
for n_neighbors in neighbors_settings:
    clf = KNeighborsClassifier(n_neighbors=n_neighbors)
    clf.fit(X_train, y_train)
    training_accuracy.append(clf.score(X_train, y_train))
    test_accuracy.append(clf.score(X_test, y_test))

# %%
# Visualize results - to help with deciding which n_neigbors yields the best results (n_neighbors=6, in this case)
plt.figure(figsize=(15, 10))

plt.plot(neighbors_settings, training_accuracy, label="Accuracy of the training set")

plt.plot(neighbors_settings, test_accuracy, label="Accuracy of the test set")

plt.ylabel("Accuracy")
plt.xlabel("Number of Neighbors")
plt.legend()

# %%
# Best k is:
test_accuracy.index(max(test_accuracy))

# %%
clf = KNeighborsClassifier(n_neighbors=5)

clf.fit(X_train, y_train)

predictions = clf.predict(X_test)

print(predictions)
print(y_test)

# plot accuracy
equal = [predictions == y_test]
print(f"Percent correct: {np.mean(equal)}")


# %%
print("Data: ", X_test[0])

print("Predicted class: ", clf.predict(X_test[0].reshape(1, -1)))

# %% [markdown]
# ![](./images/image4.png)
#
# https://en.wikipedia.org/wiki/Confusion_matrix

# %%
print("\n\nConfusion matrix:")

confusion_matrix(y_test, predictions)

# %% [markdown]
# # Cahoot-14-3

# %%
help(confusion_matrix)

# %% [markdown]
# # As a regression?
# Instead of predicting a categorical target variable,
# predict a

# %%
from sklearn.neighbors import KNeighborsRegressor

help(KNeighborsRegressor)

# %% [markdown]
# How would we implement k-nn from scratch?
# =========================================
#
# -   What would be the macro-structure of the algorithm?
# -   What data do we need to store?
# -   When do we compute the prediction?
# -   What should our training paradigm look like?
#
# See code:
# [./knn_simple.py](./knn_simple.py)
