# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown] _cell_guid="8d7fdae8-1d31-4873-a021-d553e2c4087c" _execution_state="idle" _uuid="c28fa7775a99901a882aee31e890ea99fe796d91"
# # Data analysis on cancer data
# * https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html
#     * https://scikit-learn.org/stable/datasets/toy_dataset.html#breast-cancer-wisconsin-diagnostic-dataset
#     * https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic)
# * https://www.kaggle.com/uciml/breast-cancer-wisconsin-data/kernels?sortBy=hotness&group=everyone&pageSize=20&datasetId=180&language=Python
# * https://en.wikipedia.org/wiki/Breast_cancer

# %% [markdown]
# Read these:
# * <http://www.scipy-lectures.org/packages/scikit-learn/index.html>
# * <https://scikit-learn.org/stable/tutorial/index.html>
# * <http://scikit-learn.org/stable/tutorial/basic/tutorial.html>
# * <http://scikit-learn.org/stable/tutorial/statistical_inference/index.html>
# * <https://scikit-learn.org/stable/user_guide.html>

# %% [markdown] _cell_guid="8d7fdae8-1d31-4873-a021-d553e2c4087c" _execution_state="idle" _uuid="c28fa7775a99901a882aee31e890ea99fe796d91"
# ## Genetics
# Side note: these data are not genetic, but we will get to that.
#
# In 5% of breast cancer cases, there is a strong inherited familial risk.
#
# * Two autosomal dominant genes, BRCA1 and BRCA2, account for most of the cases of familial breast cancer.
# * Women who carry a harmful BRCA mutation have a 60% to 80% risk of developing breast cancer in their lifetimes.
# * Other associated malignancies include ovarian cancer and pancreatic cancer.
# * If a mother or a sister was diagnosed breast cancer, the risk of a hereditary BRCA1 or BRCA2 gene mutation is about 2-fold higher than those women without a familial history.
# * Commercial testing for BRCA1 and BRCA2 gene mutations has been available in most developed countries since at least 2004.
# * In addition to the BRCA genes associated with breast cancer, the presence of NBR2, near breast cancer gene 1, has been discovered, and research into its contribution to breast cancer pathogenesis is ongoing.
#
# Hereditary non-BRCA1 and non-BRCA2 breast tumors (and even some sporadic carcinomas) are believed to result from the expression of weakly penetrant but highly prevalent mutations in various genes.
# * For instance, polymorphism has been identified in genes associated to the metabolism of estrogens and/or carcinogens (Cytochrome P450, family 1, member A1, CYP1B1, CYP17A1, CYP19, Catechol-O-methyl transferase, N-acetyltransferase 2, Glutathione S-transferase Mu 1, GSTP1, GSTT, . . . ), to estrogen, androgen and vitamin D action (ESR1, AR, VDR), to co-activation of gene transcription (AIB1), to DNA damage response pathways (CHEK2, HRAS1, XRCC1, XRCC3, XRCC5).
# * Sequence variants of these genes that are relatively common in the population may be associated with a small to moderate increased relative risk for breast cancer.
# * Combinations of such variants could lead to multiplicative effects.
# * Sporadic cancers likely result from the complex interplay between the expression of low penetrance gene(s) (risk variants) and environmental factors.
# * However, the suspected impact of most of these variants on breast cancer risk should, in most cases, be confirmed in large populations studies.
# * Indeed, low penetrance genes cannot be easily tracked through families, as is true for dominant high-risk genes.
#
# Part of the hereditary non-BRCA1 and non-BRCA2 breast tumors may be associated with rare syndromes, of which breast cancer is only one component.
# * Such syndromes result notably from mutations in TP53 (Li-Fraumeni syndrome), ATM (Ataxia Telangiectasia), STK11/LKB1(Peutz-Jeghers syndrome), PTEN (Cowden syndrome).
# * RAB11FIP1, TP53, PTEN and rs4973768 are also associated with increased risk of breast cancer. rs6504950 is associated with lower risk of breast cancer.
# * Mutations in RAD51C confer an increased risk for breast and ovarian cancer.

# %% _cell_guid="52942f7b-e58d-4275-86f0-ced1bcea06f9" _execution_state="idle" _uuid="d7dc365d2933b6675c57c98d438356e4cc1e6125"
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import time
from subprocess import check_output

print(check_output(["ls", "."]).decode("utf8"))

# %% _cell_guid="c9bd4680-5a5d-4ce5-8b85-1820d2e478d0" _execution_state="idle" _uuid="4a65810840012b075b5a359994931bec8acf9ab0"
data = pd.read_csv("data.csv")
data

# %% [markdown] _cell_guid="48131d2a-cb21-4c1f-8213-f0e78647287c" _execution_state="idle" _uuid="81d7851cbda2bc774e989259005e98999b84c0b2"
# Before making anything like feature selection, feature extraction, or classification, we start with basic data cleaning, description, and observation.
#
# Lets look at features of the data.

# %% _cell_guid="d30f1486-bb97-40d7-9125-67e6f15286dc" _execution_state="idle" _uuid="3e01972c830afa1ce55025c0b7a202d4b204dd1d"
data.head()  # head method show only first 5 rows

# %% [markdown] _cell_guid="d3b8329c-20eb-4f00-b900-ffc9278cd82a" _execution_state="idle" _uuid="16fbbd5b380761a5e22478133c1f8ead52ca7abb"
# **Note:**
#
# 1) There is an **id** that cannot be used for classificaiton
#
# 2) **Diagnosis** is our class label
#
# 3) **Unnamed: 32** feature includes NaN so we do not need it.
#
# Therefore, drop these unnecessary features.

# %% _cell_guid="60308baf-344a-41fb-8580-cef707ce5aa8" _execution_state="idle" _uuid="54600377cdbec016505dcb970bb1988afbc260a2"
# feature names as a list
col = data.columns  # .columns gives columns names in data
print(col)

# %% _cell_guid="8764b4cf-9963-4c1a-b449-059de8153e4c" _execution_state="idle" _uuid="94ea75618315ac7af54cf80a501c42b40e77ecbc"
# y includes our labels and x includes our features
y = data.diagnosis  # M (malignant) or B (benign)
my_list = ["Unnamed: 32", "id", "diagnosis"]
x = data.drop(my_list, axis=1)
x.head()

# %% _cell_guid="31ec8d06-ea25-4b34-84ca-c322b3d8a10f" _execution_state="idle" _uuid="71fecf26e957a2d670182d607aca5a7b92b4a3b6"
plt.rcParams["figure.figsize"] = (10, 10)

# %% _cell_guid="31ec8d06-ea25-4b34-84ca-c322b3d8a10f" _execution_state="idle" _uuid="71fecf26e957a2d670182d607aca5a7b92b4a3b6"
ax = sns.countplot(y, label="Count")  # M = 212, B = 357
B, M = y.value_counts()
print("Number of Benign: ", B)
print("Number of Malignant : ", M)

# %% [markdown] _cell_guid="8c7d5b2b-445c-45ec-81da-5b3853617711" _execution_state="idle" _uuid="af412eff1a8fa83f1f9fb6daaeac78cf498d589a"
# What is the impact of un-equal n?
#
# * Now we have features but,
#     * **what do they mean** and
#     * **how much do we need to know about these features**?
# * Given that these are physician/pathologist-coded, we do not need to know much about these features.
# * In analyzing the data, we should observe features like variance, standard deviation, number of sample (count) or max min values.
# * These types of information help us to understand what is going on with the data itself.
# * **Do we need standirdization or normalization before visualization, feature selection, feature extraction or classificaiton?**
# * Yes, now we will go step by step, and start with visualization.

# %% _cell_guid="c92292c2-d999-42f3-8618-73b231c163e6" _execution_state="idle" _uuid="7d909ed445dd83306413a72986cebc17d1814cc7"
x.describe()

# %% [markdown] _cell_guid="6179a010-0819-481e-8095-72ba1021fdcd" _execution_state="idle" _uuid="3edac4b24f82f00d32efe9d812aed40fb06fdbed"
# # Visualization
# * In order to visualizate data, we are going to use seaborn plots.
# * violin plot and swarm plot are nice.
# * Do not forget: we are not selecting features, but we are trying to explore data.

# %% [markdown] _cell_guid="fa50c6cf-ccb6-49fa-b4bb-e7f14a3c4a09" _execution_state="idle" _uuid="d3d8066df83b9a30087610eed09782c1dec7c4cf"
# * Before violin and swarm plotting, we should try normalization or standardization.
# * There are large differences between the values of our features.
# * I plot features in 3 groups, and each group includes 10 features.

# %% _cell_guid="d58052d6-9e8c-46f4-a2d5-b9e82b247f27" _execution_state="idle" _uuid="d640909614b5ff561e35b33e555458df70b22486"
# first ten features
data_dia = y
data = x
data_n_2 = (data - data.mean()) / (data.std())  # standardization
data = pd.concat([y, data_n_2.iloc[:, 0:10]], axis=1)

data = pd.melt(data, id_vars="diagnosis", var_name="features", value_name="value")

plt.figure(figsize=(10, 10))
sns.violinplot(
    x="features", y="value", hue="diagnosis", data=data, split=True, inner="quart"
)
plt.xticks(rotation=90)

# %% [markdown] _cell_guid="8eb078f8-63a8-405d-8cdb-10c9e8a06863" _execution_state="idle" _uuid="6c3ce53008a590504b1afef35e5edff183ed0ce4"
# Lets interpret the plots above.
# * For example, in **texture_mean** feature, median of the *Malignant* and *Benign* look like separated, so it could be good for classification.
# * However, in **fractal_dimension_mean** feature,  median of the *Malignant* and *Benign* does not look separated, so it may not provide good information for classification.

# %% _cell_guid="46ee71d3-93c1-4c2d-bb00-6995f7a1c816" _execution_state="idle" _uuid="0a18301387ce26b58a68e5a2d340b39e86c1f5e0"
# Second ten features
data = pd.concat([y, data_n_2.iloc[:, 10:20]], axis=1)

data = pd.melt(data, id_vars="diagnosis", var_name="features", value_name="value")

plt.figure(figsize=(10, 10))

sns.violinplot(
    x="features", y="value", hue="diagnosis", data=data, split=True, inner="quart"
)

plt.xticks(rotation=90)

# %% _cell_guid="58f17ef1-6530-4db8-bcd9-32691363e8a9" _execution_state="idle" _uuid="d1c4e84c3d6bda4b9ff9e284d8c790dd46980c31"
# Second ten features
data = pd.concat([y, data_n_2.iloc[:, 20:31]], axis=1)
data = pd.melt(data, id_vars="diagnosis", var_name="features", value_name="value")
plt.figure(figsize=(10, 10))

sns.violinplot(
    x="features", y="value", hue="diagnosis", data=data, split=True, inner="quart"
)

plt.xticks(rotation=90)

# %% [markdown] _cell_guid="7e3412af-5856-4cb1-922d-6a3dd6c5f238" _execution_state="idle" _uuid="80d14a320a514d9e727135db6d64155bea3ca35b"
# Lets interpret one more thing about the plot above
# * variables **concavity_worst** and **concave point_worst** look similar, but how can we decide whether they are correlated with each other or not?
# * (Not always true but, basically if the features are correlated with each other, then we can drop one of them)

# %% [markdown] _cell_guid="d6282b97-002d-4aeb-a09a-ce4ef138a1ca" _execution_state="idle" _uuid="f76a463fada43aa587f7bd035148698e71db6309"
# In order to compare two features more deeply, lets use a joint plot.
# * Look at this in joint plot below, demonstrates a strong correlation.
# * Pearsonr value estimates correlation value
#     * 1 is the highest.
#     * Therefore, 0.86 is looks enough to say that they are correlated.
# * Do not forget, we are not choosing features yet, we are just observing data.

# %% _cell_guid="47880bbb-5dbe-4836-938c-0816a03e8fb4" _execution_state="idle" _uuid="859ec665af4be178c3e36b1c2799f44c5ccef901"
sns.jointplot(
    data=x,
    x="concavity_worst",
    y="concave points_worst",
    kind="reg",
)

# %% [markdown] _cell_guid="008330ba-393e-4d31-b086-b90332a613c5" _execution_state="idle" _uuid="43de55ee2437101c0450d989216a426d6f1f30c7"
# What about three or more feauture comparision?
# * For this purpose we can use a pair grid plot.
# * We discover one more thing:
#     * **radius_worst**, **perimeter_worst** and **area_worst** are correlated, as seen in the pair grid plot below.

# %% _cell_guid="3bda33fe-daf9-4f74-acbc-d9d3c8fc83d9" _execution_state="idle" _uuid="381ecb55ced22383c96320ced2299f5da37ce4b6"
sns.set(style="white")
df = x.loc[:, ["radius_worst", "perimeter_worst", "area_worst"]]
g = sns.PairGrid(df, diag_sharey=False)
g.map_lower(sns.kdeplot, cmap="Blues_d")
g.map_upper(plt.scatter)
g.map_diag(sns.kdeplot, lw=3)

# %% [markdown] _cell_guid="c9ef0921-19bc-4d40-aa72-0bc2333bd4b4" _execution_state="idle" _uuid="f8355f83ac16e414e3b88e67b77d33ef31c3574d"
# Up to this point, we have some interesting observations.

# %% [markdown] _cell_guid="03abd05a-d67a-4bfc-b951-6394de8c6fc9" _execution_state="idle" _uuid="c3807ef7f6e17b33ae383349bdee7ebfced2a847"
# In the below swarm plot, we may see more:

# %% _cell_guid="ef378d49-8aed-4b9e-96e8-e7d2458fdd89" _execution_state="idle" _uuid="85a2413b70c1b3d69f26a2c122c22d55f930e774"
sns.set(style="whitegrid", palette="muted")
data_dia = y
data = x
data_n_2 = (data - data.mean()) / (data.std())  # standardization
data = pd.concat([y, data_n_2.iloc[:, 0:10]], axis=1)

data = pd.melt(data, id_vars="diagnosis", var_name="features", value_name="value")

plt.figure(figsize=(10, 10))
tic = time.time()

sns.swarmplot(x="features", y="value", hue="diagnosis", data=data)

plt.xticks(rotation=90)

# %% _cell_guid="428c75b6-b5d0-47e3-a568-17b5d1896c0c" _execution_state="idle" _uuid="75dfd5e9e50adceb1d42dd000ce779e79b069cce"
data = pd.concat([y, data_n_2.iloc[:, 10:20]], axis=1)
data = pd.melt(data, id_vars="diagnosis", var_name="features", value_name="value")
plt.figure(figsize=(10, 10))

sns.swarmplot(x="features", y="value", hue="diagnosis", data=data)

plt.xticks(rotation=90)

# %% _cell_guid="ee64bbc8-0431-482a-b08f-cdca43e41390" _execution_state="idle" _uuid="209e9e9120d6e889696d2d1190e663b5c1885a82"
data = pd.concat([y, data_n_2.iloc[:, 20:31]], axis=1)

data = pd.melt(data, id_vars="diagnosis", var_name="features", value_name="value")

plt.figure(figsize=(10, 10))

sns.swarmplot(x="features", y="value", hue="diagnosis", data=data)

toc = time.time()
plt.xticks(rotation=90)
print("swarm plot time: ", toc - tic, " s")

# %% [markdown] _cell_guid="5c9efa5e-4938-477e-ab9a-4b084cc0b870" _execution_state="idle" _uuid="d23e2f9b040a92feb0d7ceb8e01e74c758f5dbc3"
# * They look neat!
# * And you can see variance more clearly.
# * **in these three plots which feature looks like more clear in terms of classification.** ?
# * In my opinion **area_worst** in the last swarm plot looks like malignant and benign are seprated, not totaly but mostly.
# * Hovewer, **smoothness_se** in swarm plot 2, it looks like malignant and benign are mixed, so it may be hard to classfy while using this feature.

# %% [markdown] _cell_guid="c4c68f34-e876-4e5a-a4a7-09c07381425a" _execution_state="idle" _uuid="b46f98eb7ca8d36dc7bf1516895599524bab694d"
# **What if we want to observe all correlation between features?**

# %% _cell_guid="9e1e7d8a-bbf2-4aab-90e7-78d4c4ccf416" _execution_state="idle" _uuid="0eeb70ddffc8ac332ee076f2f6b2833a6ffddd2d"
# correlation map
f, ax = plt.subplots(figsize=(18, 18))

sns.heatmap(x.corr(), annot=True, linewidths=0.5, fmt=".1f", ax=ax)
